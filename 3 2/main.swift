//
//  main.swift
//  lesson3
//
//  Created by Tatyana Olhova on 06.03.2019.
//  Copyright © 2019 Tatyana Olkhova. All rights reserved.
//

import Foundation



/*
1. start — начало числового отрезка.
2. end — конец числового отрезка.
3. n — числовое значение.
Функция должна вернуть количество чисел в пределах заданного числового отрезка, делящихся без остатка на заданное числовое значение. При этом start <= n <= end.
*/
func count(start: Int, end: Int, n: Int) -> Int {
    if start > n || end < n {
        return -1
    }
    var count = 1
    var i = n * 2
    while i <= end {
        i = i + n
        count+=1
    }
    return count
}


print("Hello, World!")
print("\(count(start: 1, end: 55, n: 23))")
