//
//  User.swift
//  jsonCollection
//
//  Created by Tatyana Olhova on 22.03.2019.
//  Copyright © 2019 Tatyana Olkhova. All rights reserved.
//

import Foundation

class User {
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var phone: String = ""
    var website: String = ""
    
    init(data: [String : Any]) {
        
        id = data["id"] as! Int
        name = data["name"] as! String
        username = data["username"] as! String
        email = data["email"] as! String
        phone = data["phone"] as! String
        website = data["website"] as! String
    }
    
    /*
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
    "street": "Kulas Light",
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "zipcode": "92998-3874",
    "geo": {
    "lat": "-37.3159",
    "lng": "81.1496"
    }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
    "name": "Romaguera-Crona",
    "catchPhrase": "Multi-layered client-server neural-net",
    "bs": "harness real-time e-markets"
 */
}
