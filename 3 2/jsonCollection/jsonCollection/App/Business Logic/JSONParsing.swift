//
//  JSONParsing.swift
//  jsonCollection
//
//  Created by Tatyana Olhova on 22.03.2019.
//  Copyright © 2019 Tatyana Olkhova. All rights reserved.
//

import Foundation

class JSONParsing {
    
    static func getDictionaryFromJSON(nodeName: String, data: Data?)-> [String:Any]{
        do{
            let readableJSON = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as! [String:Any]
            if (readableJSON["error"] != nil) {
                print ("error : \(String(describing: readableJSON["error"]))")
                return [:]
            }
            return readableJSON[nodeName] as! [String:Any]
        }
        catch {
            print(error)
        }
        return [:]
    }
    
    static func getArrayFromJSON(nodeName: String, data: Data?)-> NSArray{
        do{
            let readableJSON = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as! [String:Any]
            if (readableJSON["error"] != nil) {
                print ("error : \(String(describing: readableJSON["error"]))")
                return []
            }
            return readableJSON[nodeName] as! NSArray
        }
        catch {
            print(error)
        }
        return []
    }
    
    static func getArrayFromJSON(data: Data?)-> NSArray{
        do{
            let readableJSON = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as! [[String:Any]]
            
            return readableJSON as NSArray
        }
        catch {
            print(error)
        }
        return []
    }
}
