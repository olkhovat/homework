//
//  JsonDataAccess.swift
//  jsonCollection
//
//  Created by Tatyana Olhova on 22.03.2019.
//  Copyright © 2019 Tatyana Olkhova. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class JsonDataAccess {
    
    static func _request(method: String, resultPerform: @escaping (DataResponse<Any>) -> Void)
    {
        DispatchQueue.global().async {
            Alamofire.request (method).responseJSON(queue: DispatchQueue.global(), options: .mutableContainers, completionHandler: {
                response in
                resultPerform(response)
            })
        }
    }
    
    static func getUsers(updateFunc: @escaping(_ users: [User]) -> Void) {
        
        _request(method: "https://jsonplaceholder.typicode.com/users", resultPerform: {
            response in
            let users = JSONParsing.getArrayFromJSON(data: response.data)
            var users_array: [User] = []
            for item in users {
                users_array.append(User(data: item as! [String:Any]))
            }  //User(data: user_info)
            DispatchQueue.main.async {
                updateFunc(users_array)
            }
        })
    }
    
}
