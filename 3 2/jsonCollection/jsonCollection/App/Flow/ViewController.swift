//
//  ViewController.swift
//  jsonCollection
//
//  Created by Tatyana Olhova on 22.03.2019.
//  Copyright © 2019 Tatyana Olkhova. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var usersCollectionView: UICollectionView!
    private var users: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usersCollectionView.delegate = self
        usersCollectionView.dataSource = self
        
        JsonDataAccess.getUsers { users in
            self.users = users
            self.usersCollectionView.reloadData()
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell",
                                                         for: indexPath) as? UserCollectionViewCell {
            cell.nameLabel.text = users[indexPath.row].name
            cell.phoneLabel.text = users[indexPath.row].phone
            cell.emailLabel.text = users[indexPath.row].email
            return cell
        }
        return UICollectionViewCell()
    }
}
