//
//  UserCollectionViewCell.swift
//  jsonCollection
//
//  Created by Tatyana Olhova on 22.03.2019.
//  Copyright © 2019 Tatyana Olkhova. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
}
